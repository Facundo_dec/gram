var express = require('express');
var multer = require('multer');
var ext = require ('file-extension');

var storage = multer.diskStorage({
	destination: function(req, file, cb) {
		cb(null, './uploads')
	},
	filename: function(req, file, cb) {
		cb(null, +Date.now() + '.' + ext(file.originalname))
	}
})

var upload = multer({storage: storage}).single('picture');

var express = require('express');

var app = express()

app.set('view engine','pug');

app.use(express.static('public'));

app.get('/', function (req, res) {
  res.render('index', { title: 'IsTheGram' });
})

app.get('/signup', function (req, res) {
  res.render('index');
})

app.get('/signin', function (req, res) {
  res.render('index', { title:'IsTheGram -  Sign In' });
})

app.get('/api/pictures', function(req, res){
	var pictures = [
	 {
	 	user: {
	 		username: 'FDecaminada',
	 		avatar: 'https://scontent.xx.fbcdn.net/v/t1.0-9/12718210_10207518788199655_7904690890333657971_n.jpg?oh=d055649d918c240957a3c1dccc1d5ac5&oe=596B862F'
	 	},
	 	url: 'office.jpg',
	 	likes: 0,
	 	liked: false,
	 	createdAt: + new Date().getTime(),
	 },
	 
	 {
	 	user: {
	 		username: 'Jessy',
	 		avatar: 'https://scontent.xx.fbcdn.net/v/t1.0-9/16996532_886019181540740_3639668178764182181_n.jpg?oh=0e47a7d2ffe1c914d109c23f61961def&oe=5929C7E1'
	 	},
	 	url: 'office.jpg',
	 	likes: 1,
	 	liked: true,
	 	createdAt: + new Date().setDate(new Date().getDate() -10),
	 },
  ];

setTimeout(() => res.send(pictures), 1000)

})

app.post('/api/pictures', function(req, res){
	upload(req, res, function(err) {
		if (err) {
			return res.send(500, "Error uploading File");
		}
		res.send('File uploaded!');
	})
})

app.get('/api/user/:username', function(req, res) {
	const user = {
		username: 'FDecaminada',
		avatar: 'http://www.urbanarg.com/wp-content/uploads/2014/08/facuuuu.jpg',
		pictures: [
			{
				id: 1,
				src: 'http://i.imgur.com/W0ltYch.jpg',
				likes: 3
			},

			{
				id: 2,
				src: 'https://ugc.kn3.net/i/origin/http://vignette1.wikia.nocookie.net/violetta/images/4/45/V2Cast.jpg/revision/latest/scale-to-width-down/670?cb=20150715163806',
				likes: 13
			},

			{
				id: 3,
				src: 'https://ugc.kn3.net/i/origin/http://vignette1.wikia.nocookie.net/violetta/images/4/45/V2Cast.jpg/revision/latest/scale-to-width-down/670?cb=20150715163806',
				likes: 33
			},

			{
				id: 4,
				src: 'https://k36.kn3.net/taringa/2/1/0/2/7/3/08/zashacoe/242.jpg?8420',
				likes: 0
			},

			{
				id: 5,
				src: 'https://ugc.kn3.net/i/origin/https://lumiere-a.akamaihd.net/v1/images/es_sol_ris_header_m_1ccb9ff9.jpeg?region=0,0,640,320',
				likes: 1
			},

			{
				id: 6,
				src: 'https://ugc.kn3.net/i/origin/http://vignette1.wikia.nocookie.net/violetta/images/4/45/V2Cast.jpg/revision/latest/scale-to-width-down/670?cb=20150715163806',
				likes: 300
			}
		]
	}

	res.send(user);
})

app.get('/:username', function(req, res){
	res.render('index', {title: `IsTheGram -  ${req.params.username}`})

})

app.get('/:username/:id', function(req, res){
	res.render('index', {title: `IsTheGram -  ${req.params.username}`})
})

app.listen(3000, function(err) {
  if (err) return console.log('Hubo un error'), process.exit(1);

  console.log('Escuchando en el puerto 3000')
})
