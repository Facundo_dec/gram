module.exports = {
	likes: '{ likes , plural, ' +
			'=0 {no likes }' +
			'=1 {# like }' +
			'other {# likes }}',
	'logout': 'Logout',
	'english': 'English',
	'spanish':  'Español',
	'signup.subheading': 'Signup to watch your friends :)',
	'signup.facebook': 'Signup with Facebook',
	'signup.text': 'Signup',
	'email': 'E-Mail',
	'username': 'Username',
	'fullname': 'Full Name',
	'password': 'Password',
	'signup.call-to-action': 'Signup',
	'signup.have-account': 'Already have an account?',
	'signin': 'Signin',
	'signin.not-have-account': 'Don\'t have an account?',
	'language' : 'Language',
	'upload-picture': 'Upload Picture',
	'upload': 'Upload',
	}

