var page = require ('page');
var empty = require('empty-element');
var template = require('./template');
var title = require('title');
var request = require('superagent')
var header = require ('../header')

page('/', header, loading , asyncload , function(ctx, next) { 
  title('IsTheGram');
  var main = document.getElementById('main-container');

  empty(main).appendChild(template(ctx.pictures));
 })

function loading(ctx, next) {
	var el = document.createElement('div');
	el.classList.add('loader');
	document.getElementById('main-container').appendChild(el);
	next();
}

async function asyncload(ctx, next){
	try {
		ctx.pictures = await fetch('/api/pictures').then(res =>  res.json())
		next();
	} catch (err) {
		return console.log(err);
	}
}



